<?php
require_once MODX_BASE_PATH . 'core/components/commontools/data/DataProcess.class.php';

class Core {
    
    public $modx;
    public $alias_yourLocation = 'yourLocation';
    public $restrictedIps = false;
    public $resourceId_registerActivation = 25;
    public $resourceId_registerActivationSuccess = 26;
    public $resourceId_registerActivationFail = 27;
    public $resourceId_registerThanks = 30;
    public $group_members = 2;
    public $group_admin = 1;
    public $resourceId_registerMemberHome = 19;
    public $resourceId_forgotPass = 28;
    public $resourceId_forgotPassReset = 29;
    public $resourceId_login = 21;

    
    function __construct($modx) {
        //To easily check if IP is allowed make an array of allowed IPs and call restrictedIpAllowed()
        //$this->restrictedIps = array('58.162.254.30','192.168.0.');
        if(empty($modx) || get_class($modx)!='modX'){
            $this->throwError('$modx not correctly defined.');
        }
        
	$this->modx = $modx;
    }
    
    public function show404(){
        header("HTTP/1.0 404 Not Found");
        $i_errorPage = $this->modx->getOption('error_page', null);
        $this->modx->sendForward($i_errorPage);
        exit();
    }
    
    public function refreshXpdoTablesIfNeeded(){
        $s_refreshVal = $this->getVal('refreshDataTables');
        $package = '****package****';
        $tableprefix = '****tableprefix****';
        $tables = array('origsiteinfo','siteinfo','safetyinfo');
        if(!empty($s_refreshVal)){
            $s_componentsPath = MODX_CORE_PATH.'components/';
            $a_filesToDelete=array(
                $s_componentsPath.$package.'/schema/'.$package.'.mysql.schema.xml'
            );
            foreach($tables as $table){
                $a_filesToDelete[]=$s_componentsPath.$package.'/model/'.$package.'/mysql/'.$table.'.class.php';
                $a_filesToDelete[]=$s_componentsPath.$package.'/model/'.$package.'/'.$table.'.class.php';   
            }

            foreach($a_filesToDelete as $s_file){ 
                
                @unlink($s_file);
            }
            echo $this->modx->runSnippet('CreateXpdoClasses', array(
                'myPackage' => $package,
                'myPrefix' => $tableprefix
            ));
            exit();
        }
    }
    
    public function isAdminLoggedIn() {
        $usergroups = $this->modx->user->getUserGroups();
        if (in_array($this->group_admin, $usergroups) === true) {
            return true;
        } else {
            return false;
        }
    }
    
    public function isMemberLoggedIn() {
        $usergroups= $this->modx->user->getUserGroups();
        if (in_array($this->group_members, $usergroups) === true) {
            return true;
        }else {
            return false;
        }
    }
    
    public function restrictedIpAllowed(){
        foreach($this->restrictedIps as $ip){
            if(strpos($_SERVER['REMOTE_ADDR'],$ip)===0){
                return true;
            }
        }
        return false;
    }
    
    public function migxToCsv($s_migxData){
        $o_dataProcessor = new DataProcess();  
        
        $a_locations = json_decode($s_migxData, true);
        
        $a_dataRows=array();
        $a_header=array();
        foreach($a_locations[0] as $key=>$val){
            $a_header[]=$key;
        }
        $a_dataRows[]=$a_header;
        
        foreach($a_locations as $loc){
            $a_row = array();
            foreach($loc as $key=>$val){
                $a_row[]=$val;
            }
            $a_dataRows[]=$a_row;
        }
        
        $processResult = $o_dataProcessor->arrayToCsv($a_dataRows,$delimiter=',');
        
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=forest-activities.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo $processResult;
        exit();
    }
    
    public function makehash($len) {
        $alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $len; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    public function getVal($name) {
        $a = $_GET; //cant use filter_input because it doesn't allow modification of arrays
        if(empty($a[$name])===true && $a[$name]!=0){
            return false;
        }else{
            return trim($a[$name]);
        }
    }
    public function postVal($name) {
        $a = $_POST; //cant use filter_input because it doesn't allow modification of arrays
        if(empty($a[$name])===true && $a[$name]!=0){
            return false;
        }else{
            return trim($a[$name]);
        }
    }
    public function postOrGetVal($name) {
       $get = $this->getVal($name);
       if($get==''){
           return $this->postVal($name);
       }
       return $get;
    }
    public function throwError($str){
        throw new Exception($str);
    }
    public function getRewriteArray() {
	$a_customStartPaths = array(
            array('startPath'=>$this->alias_yourLocation, 'resourceId'=>1),
	);
	$q = $this->getVal('q');
	if($q){
	    $split = explode('/', $q);
	    foreach ($a_customStartPaths as $path) {
		if ($split[0] == $path['startPath']) {
		    return array('resourceId'=>$path['resourceId'],'split'=>$split);
		}
	    }
	}
	return false;
    }
    public function handleRewrites() {
	$a = $this->getRewriteArray();
	if($a){
	    $this->modx->sendForward($a['resourceId']);
	}
    }
    public function strCut($chars,$str){
        if(strlen($str)>$chars){
            return substr($str, 0, $chars).'…';
        }else{
            return $str;
        }
    }
}
