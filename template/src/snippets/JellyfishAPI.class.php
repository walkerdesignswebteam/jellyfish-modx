<?php
//require_once 'Core.class.php';

class JellyfishAPI extends Core{
    
    public $s_xlsxDataFile = null;
    public $s_kmlDataFile = null;
    public $a_jellyfishData = null;
    public $a_locationData = null;
    
    private $cacheFolder;
    private $jellyfishProcessedPhpFile;
    private $locationsProcessedPhpFile;
    private $s_baseImagePath;
    private $a_emailFields = array('email','name','notes');
    
    private $i_blogResourceID = 11;
    private $i_aboutLisa = 13;
    private $i_faq = 3;
    
    public function __construct($modx){
        parent::__construct($modx);
        $this->setConfig();
        $this->cacheFolder = MODX_CORE_PATH.'cache/jellyfish/';
        if(!file_exists($this->cacheFolder)){
            mkdir($this->cacheFolder,0755,true);
        }
        $this->jellyfishProcessedPhpFile= $this->cacheFolder.'jellyfishdata.php';
        $this->locationsProcessedPhpFile= $this->cacheFolder.'locationdata.php';
        $this->s_baseImagePath = MODX_BASE_PATH.'client-assets/images/';
    }   
    
    public function output(){
        $cmd = $_POST['cmd'];
        if(empty($cmd)){
            $cmd = $_GET['cmd'];
        }
        $ret = '';
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        switch($cmd){
            case 'jellyfish':
                $data = $this->loadJFData();
                $ret = json_encode($data['jellyfish']['data']);
                break;
            case 'last_update':
                $data = $this->loadJFData();
                $ret = $data['jellyfish']['last_update'];
                break;
            case 'locations':
                $data = $this->loadLocationData();
                $ret = json_encode($data['data']);
                break;
            case 'loc_last_update':
                $data = $this->loadLocationData();
                $ret = $data['last_update'];
                break;
            case 'ask-lisa':
                $this->askLisa();
                break;
            case 'bloom-blog':
                $ret = json_encode($this->bloomBlog());
                break;
            case 'check-last-mod':
                $ret = json_encode($this->checkLastMod());
                break;
            case 'about-lisa':
                $ret = json_encode($this->aboutLisaContent());
                break;
            case 'faq':
                $ret = json_encode($this->faqContent());
                break;
            default:
                break;
        }
        echo $ret;
        exit();
    }
    
    private function askLisa(){
        error_log('Asking Lisa');
        $a_data = array();
        foreach($this->a_emailFields as $field){
            $a_data[$field] = $_POST[$field];
            error_log('Received data: '.$field.', with value: '.$a_data[$field]);
        }
        
        $s_body = $this->modx->getChunk('emailBody_tpl',$a_data);
        $s_tempName = '';
        $s_fileName = '';
        if(array_key_exists('image', $_FILES)){
            $image = $_FILES['image'];
            $s_tempName = $image['tmp_name'];
            $s_fileName = $image['name'];
        }
        
        if(!empty($a_data['email'])){
            $this->sendEmail($a_data, $s_body,'Ask Lisa Submission from: '.$a_data['name'],$s_tempName,$s_fileName);
        }
    }
    
    private function sendEmail($a_details, $message, $subject, $s_attachmentName='', $s_fileName=''){
        error_log('Sending email');
        $b_ret = false;
        $mailFrom = $this->modx->getOption('emailsender');
        $mailSender = 'The Jellyfish App';

        $this->modx->getService('mail', 'mail.modPHPMailer');
        $this->modx->mail->set(modMail::MAIL_BODY,$message);
        $this->modx->mail->set(modMail::MAIL_FROM,$mailFrom);
        $this->modx->mail->set(modMail::MAIL_FROM_NAME,$mailSender);
        $this->modx->mail->set(modMail::MAIL_SENDER,$mailFrom);
        $this->modx->mail->set(modMail::MAIL_SUBJECT,$subject);

        $this->modx->mail->address('to',$this->modx->getOption('emailsender'));

        if($s_attachmentName != '' && file_exists($s_attachmentName)){
            error_log('Added attachment');
            $this->modx->mail->mailer->AddAttachment($s_attachmentName,$s_fileName);
        }else{
            error_log('Failed to add attachment');
        }
        
        $this->modx->mail->setHTML(true);

        if (!$this->modx->mail->send()) {
            error_log('Failed to send email');
            $this->modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$this->modx->mail->mailer->ErrorInfo);
        }else{
            error_log('Email sent');
            $b_ret = true;
        }
        $this->modx->mail->reset();
        return $b_ret;
    }

    private function loadJFData(){
        if(!file_exists($this->jellyfishProcessedPhpFile) || filemtime($this->s_xlsxDataFile) > filemtime($this->jellyfishProcessedPhpFile)){
            $this->getJellyfishData();
        }
        require $this->jellyfishProcessedPhpFile;
        return $jellyfishData;
    }
    
    private function loadLocationData(){
        if(!file_exists($this->locationsProcessedPhpFile) || filemtime($this->s_kmlDataFile) > filemtime($this->locationsProcessedPhpFile)){
            $this->getLocationData();
        }
        require $this->locationsProcessedPhpFile;
        return $locationData;
    }
    
    private function crossesDateLine($a_points){
    $count = count($a_points);
    for($i=0;$i<$count-1;$i++){
        $a_point1 = $a_points[$i];
        $a_point2 = $a_points[$i+1];
        if(!(($a_point1['lon'] >= 0 && $a_point2['lon'] >= 0) || ($a_point1['lon'] < 0 && $a_point2['lon'] < 0))){
            $dx = $a_point1['lon'] - $a_point2['lon'];
            $abs = abs($dx);
            if($abs > 180){
                return true;
            }
        }
    }
    return false;
}

private function parsePoly($coordinates){
    $points = array();
    $a_coords = explode(' ', trim($coordinates));
    foreach($a_coords as $coord){
        $a_latLong = explode(',',$coord);
        if($a_latLong[1] != '' && $a_latLong[0] != ''){
            $correctedLat = $this->correctLat($a_latLong[1]);
            $correctedLon = $this->correctLon($a_latLong[0]);

            $b_corrected = false;
            if($correctedLat !== $a_latLong[1] || $correctedLon !== $a_latLong[0]){
                $b_corrected = true;
            }

            $points[] = array('lat'=>$correctedLat,'lon'=>$correctedLon,'rawlat'=>$a_latLong[1],'rawlon'=>$a_latLong[0],'corrected'=>$b_corrected);
        }
        //$points[] = array('lon'=>$a_latLong[0],'lat'=>$a_latLong[1]);
    }
    return $points;
}


    private function getLocationData(){
        $o_kml = simplexml_load_file($this->s_kmlDataFile);
        $a_coordinates = array();
        $outerContainer = $o_kml->Document->Folder;
            foreach($outerContainer as $container){
                foreach($container->Placemark as $placemark){
                    $name = (string) $placemark->name; 
                    $coordinates = $placemark->Polygon->outerBoundaryIs->LinearRing->coordinates;
                    $a_latLongs = $this->parsePoly($coordinates);
                    $points = array();
                    $b_crossesDateLine = $this->crossesDateLine($a_latLongs);
                    //$a_coords = explode(' ', trim($coordinates));
                    foreach($a_latLongs as $a_latLong){
                        //$a_latLong = explode(',',$coord);

                        if($a_latLong['lat'] != '' && $a_latLong['lon'] != ''){
                            if($b_crossesDateLine && $a_latLong['lon'] < 0){
                                $shiftedLon = $a_latLong['lon']+360;
                            }else{
                                $shiftedLon = $a_latLong['lon'];
                            }
                            
                            $points[] = array('lat'=>$a_latLong['lat'],'lon'=>$a_latLong['lon'],'rawlat'=>$a_latLong['rawlat'],'rawlon'=>$a_latLong['rawlon'],'corrected'=>$a_latLong['corrected'],'shifted_lon'=>$shiftedLon);
                        }
                    }
                    //error_log('Location name: '.$name);
                    $a_location = array('name'=>$name,'crosses_date_line'=>$b_crossesDateLine,'coords'=>$points);
                    $a_coordinates[] = $a_location;
                }

            }
        $this->a_locationData = array('last_update'=>  filemtime($this->s_kmlDataFile),'data'=>$a_coordinates);
        $s_phpDataContent = var_export($this->a_locationData,true);
        
        $s_phpFileContent = '<?php'."\r\n".'$locationData='.$s_phpDataContent.';';
        
        error_log('Saving cache file to: '.$this->locationsProcessedPhpFile);
        
        if(file_put_contents($this->locationsProcessedPhpFile,$s_phpFileContent)){
            error_Log('Successfully save cache file');
        }else{
            error_Log('Failed to save cache file');
        }
        
    }
    
/*
    private function getLocationData(){
        $o_kml = simplexml_load_file($this->s_kmlDataFile);
        $a_coordinates = array();
        $outerContainer = $o_kml->Document->Folder;
            foreach($outerContainer as $container){
                foreach($container->Placemark as $placemark){
                    $name = (string) $placemark->name; 
                    $coordinates = $placemark->Polygon->outerBoundaryIs->LinearRing->coordinates;
                    $points = array();
                    $a_coords = explode(' ', trim($coordinates));
                    foreach($a_coords as $coord){
                        $a_latLong = explode(',',$coord);

                        if($a_latLong[1] != '' && $a_latLong[0] != ''){
                            $correctedLat = $this->correctLat($a_latLong[1]);
                            $correctedLon = $this->correctLon($a_latLong[0]);
                            
                            $b_corrected = false;
                            if($correctedLat !== $a_latLong[1] || $correctedLon !== $a_latLong[0]){
                                $b_corrected = true;
                            }
                            
                            $points[] = array('lat'=>$correctedLat,'lon'=>$correctedLon,'rawlat'=>$a_latLong[1],'rawlon'=>$a_latLong[0],'corrected'=>$b_corrected);
                        }
                    }
                    //error_log('Location name: '.$name);
                    $a_location = array('name'=>$name,'coords'=>$points);
                    $a_coordinates[] = $a_location;
                }

            }
        $this->a_locationData = array('last_update'=>  filemtime($this->s_xlsxDataFile),'data'=>$a_coordinates);
        $s_phpDataContent = var_export($this->a_locationData,true);
        
        $s_phpFileContent = '<?php'."\r\n".'$locationData='.$s_phpDataContent.';';
        
        error_log('Saving cache file to: '.$this->locationsProcessedPhpFile);
        
        if(file_put_contents($this->locationsProcessedPhpFile,$s_phpFileContent)){
            error_Log('Successfully save cache file');
        }else{
            error_Log('Failed to save cache file');
        }
        
    }
    */
    private function correctLat($lat){
        if($lat > 90){
            $lat = 90;
        }else if($lat < -90){
            $lat = -90;
        }
        return $lat;
    }
    
    private function correctLon($lon){
        if($lon > 180){
            $lon -= 360;
        }else if($lon < -180){
            $lon += 360;
        }
        return $lon;
    }
    
    private function setConfig(){
        $this->s_xlsxDataFile = MODX_BASE_PATH.$this->modx->resource->getTVValue('jellyfishData');
        $this->s_kmlDataFile = MODX_BASE_PATH.$this->modx->resource->getTVValue('locationData');
    }
    
    public function getJellyfishData(){
        require_once 'spreadsheet-reader/php-excel-reader/excel_reader2.php';
        require_once 'spreadsheet-reader/SpreadsheetReader.php';
        $o_spreadsheetReader = new SpreadsheetReader($this->s_xlsxDataFile);
        //Use to debug sheet indexes
        $sheets = $o_spreadsheetReader->Sheets();
        $i_jellyfishSheet = false;
        foreach($sheets as $index=>$sheet){
            switch(strtolower($sheet)){
                case 'sheet1': $i_jellyfishSheet = $index; break;
            }
        }
        if($i_jellyfishSheet===false){ echo 'Cannot find Jellyfish sheet in excel file.'; exit(); }
        
        //Make assoc array from header row in first sheet (other sheets ignored)
        $o_spreadsheetReader->ChangeSheet($i_jellyfishSheet);
        $a_jellyfishData=array();
        $a_headerRow = false;
        $i_genusIndex;
        $i_speciesIndex;
        
        $s_thumboptions = 'f=jpg&zc=1&w=300&h=200';
        $s_imageoptions = 'f=jpg&zc=1&w=600&h=400';
                
        foreach ($o_spreadsheetReader as $rownum=>$rowdata){
            if($rownum==0){
                $a_headerRow = $rowdata;
                $i_genusIndex = array_search('genus', $rowdata);
                $i_speciesIndex = array_search('species', $rowdata);
            }else{
                if($rowdata){
                    $a_processedRow = array();
                    $a_processedRow['properties'] = array();
                    for($i=0;$i<count($rowdata);$i++){
                        if(!empty($a_headerRow[$i])){
                            $headerName = $this->cleanHeader($a_headerRow[$i]);
                            $a_processedRow[$headerName]=$rowdata[$i];
                            $a_processedRow['properties'][] = $headerName;
                        }
                    }
                    $s_imagePath = $this->s_baseImagePath.$this->cleanFolderName($rowdata[$i_genusIndex]).'/'.$this->cleanFolderName($rowdata[$i_speciesIndex]).'/';
                        //echo 'Base path: '.$s_imagePath.'<br />';
                        $a_rawImages = $this->getFileList($s_imagePath);
                        
                        //print_r($a_rawImages);
                        //echo '<br />';
                        //exit;
                        
                        $a_images = array();
                        
                        foreach($a_rawImages as $image){
                            $a_img = array();
                            $s_image = $s_imagePath.$image;
                            $a_img['thumb'] = $this->modx->runSnippet('phpthumbof',array('input'=>$s_image,'options'=>$s_thumboptions));
                            $a_img['image'] = $this->modx->runSnippet('phpthumbof',array('input'=>$s_image,'options'=>$s_imageoptions));
                            
                            $s_thumb = urldecode($a_img['thumb']);
                            if(substr($a_img['thumb'], 0,1) === '/'){
                                $s_thumb = MODX_BASE_PATH.urldecode(substr($a_img['thumb'], 1));
                            }
                            
                            $a_img['filemtime'] = filemtime($s_thumb);
                            $a_images[] = $a_img;
                        }
                        
                        $a_processedRow['images'] = $a_images;
                    if(!empty($a_processedRow['sp_no'])){
                        $a_jellyfishData[]=$a_processedRow;
                    }
                }
            }
        }
        //exit;
        $this->a_jellyfishData = array('jellyfish'=>array('last_update'=>  filemtime($this->s_xlsxDataFile),'data'=>$a_jellyfishData));
        $s_phpDataContent = var_export($this->a_jellyfishData,true);
        //echo 'Data file path: '.$this->jellyfishProcessedPhpFile.'<br />';
        
        $s_phpFileContent = '<?php'."\r\n".'$jellyfishData='.$s_phpDataContent.';';
        if(file_put_contents($this->jellyfishProcessedPhpFile,$s_phpFileContent)){
            //echo 'Successfully wrote data to file<br />';
        }else{
            //echo 'Failed to write data to file<br />';
        }
        
    }
    
    private function cleanHeader($s_header){
        $s_header = str_replace('/','_',$s_header);
        return str_replace('©','',$s_header);
    }
    
    private function cleanFolderName($s_name){
        return str_replace('.', '', trim($s_name));
    }
    
    private function getFileList($path){
        $a_files = scandir($path);
        $i1 = array_search('.',$a_files);
        unset($a_files[$i1]);
        $i2 = array_search('..',$a_files);
        unset($a_files[$i2]);
        return $a_files;
    }
    
    private function checkLastMod(){
        $jf_data = $this->loadJFData();
        $loc_data = $this->loadLocationData();
        $a_ret = array(array('jellyfish'=>$jf_data['jellyfish']['last_update'],'locations'=>$loc_data['last_update'],'blog'=>$this->lastBlog(),'faq'=>$this->lastFaq(),'about-lisa'=>$this->lastAboutLisa()));
        return $a_ret;
    }
   
    private function lastBlog(){
        $query = $this->modx->newQuery('modResource');
        $query->where(array('parent'=>$this->i_blogResourceID));
        $query->sortBy('publishedon','DESC');
        $query->limit(1);
        $o_resource = $this->modx->getObject('modResource', $query);
        return $o_resource->publishedon;
    }
    
    private function bloomBlog(){
        $o_blogResource = $this->modx->getObject('modResource', $this->i_blogResourceID);
        $query = $this->modx->newQuery('modResource');
        $query->where(array('parent'=>$this->i_blogResourceID));
        $query->sortBy('publishedon','DESC');
        $query->limit(6);
        $c_resources = $this->modx->getCollection('modResource', $query);
        $a_return = array();
        $s_url = $this->modx->makeUrl($this->i_blogResourceID,'','',array('scheme'=>'abs'));
        $a_return['info'] = array('name'=>$o_blogResource->pagetitle,'url'=>$s_url);
        $a_return['data'] = array();
        $count = 0;
        foreach($c_resources as $o_resource){
            if($count == 0){
                $a_return['info']['lastmod'] = $o_resource->publishedon;
            }
            $s_url = $this->modx->makeUrl($o_resource->id,'','',array('scheme'=>'abs'));
            $s_imageSrc = $o_resource->getTVValue('image');
            $s_img = $this->modx->runSnippet("phpthumbof",array('input'=>$s_imageSrc, 'options'=>'w=125 &h=130 &q=65 &f=jpg'));
            $a_return['data'][] = array('id'=>$o_resource->id,'title'=>$o_resource->pagetitle,'content'=>strip_tags ( $o_resource->content),'publishedon'=>$o_resource->publishedon,'url'=>$s_url,'image'=>$s_img);
            $count++;
        }
        return array($a_return);
    }
    
    private function aboutLisaContent(){
        $o_resource = $this->modx->getObject('modResource',$this->i_aboutLisa);
        $s_imageSrc = $o_resource->getTVValue('image');
        $s_img = $this->modx->runSnippet("phpthumbof",array('input'=>$s_imageSrc, 'options'=>'w=125 &h=130 &q=65 &f=jpg'));
        $a_ret = array(array('title'=>$o_resource->pagetitle,'content'=>strip_tags ($o_resource->content),'image'=>$s_img,'lastmod'=>$o_resource->editedon));
        return $a_ret;
    }
    
    private function lastAboutLisa(){
        $o_resource = $this->modx->getObject('modResource',$this->i_aboutLisa);
        return $o_resource->editedon;
    }
    
    private function faqContent(){
        $o_resource = $this->modx->getObject('modResource', $this->i_faq);
        $s_faq = $o_resource->getTVValue('questions');
        $a_faq = json_decode($s_faq);
        $a_ret = array('lastmod'=>$o_resource->editedon,'data'=>array());
        foreach($a_faq as $faq){
            $a_ret['data'][] = array('id'=>$faq->MIGX_id,'question'=>$faq->question,'answer'=>$faq->answer);
        }
        return array($a_ret);
    }
    
    private function lastFaq(){
        $o_resource = $this->modx->getObject('modResource',$this->i_faq);
        return $o_resource->editedon;
    }
}
