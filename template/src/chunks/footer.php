<footer>
	<div class="row">
		<div class="col-xs-12">
			<p>&copy; 2015 [[++site_name]]. All rights reserved.</p>
			<p>Website by <a href="http://walkerdesigns.com.au/web/" rel="external" title="Website design, development, hosting and copywriting by Walker Designs, Launceston, Hobart, Tasmania, Australia">Walker Designs</a>
			</p>
		</div>
	</div>
</footer>